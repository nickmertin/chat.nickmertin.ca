import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import { init, close } from './live'

Vue.use(Vuex)

export interface State {
  authToken: null | string
  admin: boolean
  navError: any
  ws: boolean
}

const store = new Store({
  state: <State>{
    authToken: null,
    admin: false,
    navError: null,
    ws: false
  },
  getters: {
    authenticated (state) {
      return state.authToken !== null
    }
  },
  mutations: {
    updateAuth (state, { token, admin }) {
      state.authToken = token
      state.admin = admin
      if (token) {
        localStorage.setItem('token', token)
        init(store)
      } else {
        localStorage.removeItem('token')
        close()
      }
    },
    updateWs (state, ws) {
      state.ws = ws
    },
    setNavError (state, error) {
      state.navError = error
    }
  }
})

export default store
