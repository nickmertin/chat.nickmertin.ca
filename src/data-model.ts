export interface ConversationEntry {
  group: boolean
  target: string
  preview: string
  timestamp: string
}
