import config from './config'
import { Store } from 'vuex'
import { State } from './store'
import { delay } from './utils'
import router from './router'

export type MessageType = 'direct' | 'group'
export type MessageHandler = (type: MessageType, target: string, id: number, text: string) => boolean

interface Packet {
  id: number
  content: string
}

var ws: WebSocket | null = null
var retry = true
const handlers = new Set<MessageHandler>()
const queue: Packet[] = []
var counter = 0

function wrap(packet: Packet) {
  return `${packet.id.toString(16)}+${packet.content}`
}

export function init(store: Store<State>) {
  retry = true
  if (ws !== null) {
    ws.close()
  }
  ws = new WebSocket(config.wsUrl, store.state.authToken!)
  ws.onopen = () => store.commit('updateWs', true)
  ws.onmessage = e => {
    const data = /^([0-9a-f]+)\+(\w+):(.*)$/i.exec(e.data)
		if (!data) {
      if (/^[0-9a-f]+$/i.test(e.data)) {
        const packet = queue.filter(p => p.id === parseInt(e.data, 16))[0]
        if (packet) {
          queue.splice(queue.indexOf(packet), 1)
        }
      }
      return
    }
    ws!.send(data[1])
    cmd: switch (data[2]) {
      case 'auth':
        switch (data[3]) {
          case 'invalid_token':
            retry = false
            store.commit('updateAuth', {
              token: null,
              admin: false
            })
            router.push('/login/' + encodeURIComponent(router.currentRoute.fullPath))
            break
          case 'good':
            for (const packet of queue.sort((a, b) => a.id - b.id)) {
              ws!.send(wrap(packet))
            }
            queue.length = 0
            break
        }
        break
      case 'message':
        let [type, target, id, text]: any = /^(\w+)\/([^/]+)\/(\d+)\/(.*)$/.exec(data[3])!.slice(1)
        target = decodeURIComponent(target)
        id = parseInt(id)
        for (const handler of handlers) {
          if (handler(type, target, id, text)) {
            break cmd
          }
        }
        alert((<any>{
          direct: () => `Direct message from ${target}: ${text}`,
          group: () => `Message in group ${target}: ${text}`
        })[type]())
        break
    }
  }
  ws.onerror = e => {
    console.log(e)
  }
  ws.onclose = async () => {
    store.commit('updateWs', false)
    console.log('Close; retrying: ' + (retry ? 'yes' : 'no'))
    if (retry) {
      await delay(500)
      init(store)
    }
  }
}

export function close() {
  ws && ws.close()
  retry = false
  ws = null
}

export function send(message: string) {
  const packet = {
    id: counter++,
    content: message
  }
  queue.push(packet)
  ws && ws.send(wrap(packet))
}

export function addMessageHandler(handler: MessageHandler) {
  handlers.add(handler)
}

export function removeMessageHandler(handler: MessageHandler) {
  handlers.delete(handler)
}
