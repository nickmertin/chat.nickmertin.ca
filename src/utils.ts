import Vue from 'vue'

export function nextTick() {
  return new Promise<void>(resolve => Vue.nextTick(resolve))
}

export function delay(timeout: number) {
  return new Promise<void>(resolve => window.setTimeout(resolve, timeout))
}
