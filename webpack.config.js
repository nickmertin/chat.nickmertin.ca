const path = require('path')
const VueLoaderPlgin = require('vue-loader/lib/plugin')

module.exports = {
  mode: process.env.NODE_ENV,
  entry: './src/main.ts',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'chat.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.ts$/,
        use: [
          'babel-loader',
          {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.(mpct|vue)$/]
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /assets|\.ttf|\.woff2?|\.eot|\.svg/,
        exclude: /\.css$/,
        loader: 'file-loader',
        options: {
          name: 'assets/[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.ts', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map',
  plugins: [
    new VueLoaderPlgin()
  ],
  node: {
    fs: 'empty'
  }
}

if (process.env.NODE_ENV === 'production') {
  delete module.exports.devtool
  Object.assign(module.exports, {
    optimization: {
      minimize: true,
      noEmitOnErrors: true
    }
  })
}
