import Vue, { VueConstructor } from 'vue'
import BootstrapVue from 'bootstrap-vue'
import { sync } from 'vuex-router-sync'
import store from './store'
import router from './router'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import * as $ from 'jquery'
import components from './components'
import config from './config'
import './assets/style.css'

$.extend(window, { $ })

Vue.use(BootstrapVue)

console.log(components)

for (const name in components) {
  Vue.component(name, (<ObjectMap<VueConstructor>>components)[name])
}

(async () => {
  const storedToken = localStorage.getItem('token')
  if (storedToken) {
    try {
      const data = await $.ajax(`${config.apiBase}/verify`, {
        method: 'POST',
        data: {
          token: storedToken
        }
      })
      store.commit('updateAuth', {
        token: storedToken,
        admin: data.admin
      })
    } catch (_) {
      localStorage.removeItem('token')
    }
  }
  new App({
    store,
    router
  }).$mount('#app')  
})()

sync(store, router)
