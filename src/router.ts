import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './pages/Home.vue'
import Login from './pages/Login.vue'
import Logout from './pages/Logout.vue'
import Error from './pages/Error.vue'
import Admin from './pages/Admin.vue'
import Admin_AddUser from './pages/Admin_AddUser.vue'
import Conversations from './pages/Conversations.vue'
import Conversations_Direct from './pages/Conversations_Direct.vue'
import store from './store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login/:next',
      component: Login
    },
    {
      path: '/logout',
      component: Logout
    },
    {
      path: '/error',
      component: Error
    },
    {
      path: '/admin',
      component: Admin
    },
    {
      path: '/admin/add-user',
      component: Admin_AddUser
    },
    {
      path: '/conversations',
      component: Conversations
    },
    {
      path: '/conversations/direct/:other',
      component: Conversations_Direct
    }
  ]
})

export default router

router.beforeEach((to, from, next) => {
  if (!/^\/login(?:\/.*)?$/.test(to.path) && !store.getters.authenticated) {
    next('/login/' + encodeURIComponent(to.fullPath))
  } else if (/^\/admin(?:\/.*)?$/.test(to.path) && !store.state.admin) {
    store.commit('setNavError', 'This page requires admin privledges: ' + to.path)
    next('/error')
  } else if (!to.matched.length) {
    store.commit('setNavError', 'Could not find page: ' + to.path)
    next('/error')
  } else {
    next()
  }
})

router.onError((error: any) => {
  store.commit('setNavError', error)
  router.replace('/error')
})
