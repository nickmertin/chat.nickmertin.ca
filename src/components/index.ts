import LoadingSpinner from './LoadingSpinner.vue'
import ConversationItem from './ConversationItem.vue'
import * as spinners from 'epic-spinners'

export default {
  ...spinners,
  LoadingSpinner,
  ConversationItem
}
